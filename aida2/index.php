<!DOCTYPE html>
<html lang="en">

<head>
<style> 
b {
  animation-duration: 3s;
  animation-name: slidein;
}

@keyframes slidein {
  from {
    margin-left: 100%;
    width: 300%; 
  }

  to {
    margin-left: 0%;
    width: 100%;
  }
}
select.list1 option.option2
{
    background-color: #007700;
}

font 
{
color:white;
}
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Casfer Design </title>

	<link rel="stylesheet" href="animate.min.css" />
	<link rel="stylesheet" href="lightbox.css">
    <link rel="stylesheet" href="css/effect.min.css" type="text/css">
    <link rel="stylesheet" href="css/animation.min.css" type="text/css">
    <link rel="stylesheet" href="css/design.css" type="text/css">
	<script
		src="http://maps.googleapis.com/maps/api/js">
		</script>
		<script src="common.js"></script> 
	<script src="clienthint.js"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript">
	



function loadXMLDoc(url)
{
if (window.XMLHttpRequest)
  {
  xmlhttp=new XMLHttpRequest();
  }
else
  {
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    txt="<table align='center' border='3'><tr><th>CAST</th><th>CHARACTER</th></tr>";
    x=xmlhttp.responseXML.documentElement.getElementsByTagName("CD");
    for (i=0;i<x.length;i++)
      {
      txt=txt + "<tr>";
      xx=x[i].getElementsByTagName("CAST");
        {
        try
          {
          txt=txt + "<td>" + xx[0].firstChild.nodeValue + "</td>";
          }
        catch (er)
          {
          txt=txt + "<td> </td>";
          }
        }
      xx=x[i].getElementsByTagName("CHARACTER");
        {
        try
          {
          txt=txt + "<td>" + xx[0].firstChild.nodeValue + "</td>";
          }
        catch (er)
          {
          txt=txt + "<td> </td>";
          }
        }
      txt=txt + "</tr>";
      }
    txt=txt + "</table>";
    document.getElementById('XML').innerHTML=txt;
    }
  }
xmlhttp.open("GET",url,true);
xmlhttp.send();
}
</script>


</head>

<body id="page-top">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">MVC drama blog</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#videos">Videos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#wallpapers">Wallpapers</a>
                    </li>
					 <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="character.html">Characters</a>
                    </li>
                </ul>
            </div>

        </div>

    </nav>

    <header class="site__header island">
        <div class="header-content">
            <div class="header-content-inner">
			<span id="animationSandbox" style="display: block;"><h1 class="site__title mega">Vampire Diaries</h1></span>
		
                <div class="wrap">
    <form>
	
     <strong><font size="3">Select: </font>	</strong><select class="list1 input--dropdown js--animations">
        <optgroup label="Attention Seekers">
          <option value="bounce">bounce</option>
          <option value="flash">flash</option>
          <option value="pulse">pulse</option>
          <option value="rubberBand">rubberBand</option>
          <option value="shake">shake</option>
          <option value="swing">swing</option>
          <option value="tada">tada</option>
          <option value="wobble">wobble</option>
          <option value="jello">jello</option>
        </optgroup>

        <optgroup label="Bouncing Entrances">
          <option value="bounceIn">bounceIn</option>
          <option value="bounceInDown">bounceInDown</option>
          <option value="bounceInLeft">bounceInLeft</option>
          <option value="bounceInRight">bounceInRight</option>
          <option value="bounceInUp">bounceInUp</option>
        </optgroup>

        <optgroup label="Bouncing Exits">
          <option value="bounceOut">bounceOut</option>
          <option value="bounceOutDown">bounceOutDown</option>
          <option value="bounceOutLeft">bounceOutLeft</option>
          <option value="bounceOutRight">bounceOutRight</option>
          <option value="bounceOutUp">bounceOutUp</option>
        </optgroup>

        <optgroup label="Fading Entrances">
          <option value="fadeIn">fadeIn</option>
          <option value="fadeInDown">fadeInDown</option>
          <option value="fadeInDownBig">fadeInDownBig</option>
          <option value="fadeInLeft">fadeInLeft</option>
          <option value="fadeInLeftBig">fadeInLeftBig</option>
          <option value="fadeInRight">fadeInRight</option>
          <option value="fadeInRightBig">fadeInRightBig</option>
          <option value="fadeInUp">fadeInUp</option>
          <option value="fadeInUpBig">fadeInUpBig</option>
        </optgroup>

        <optgroup label="Fading Exits">
          <option value="fadeOut">fadeOut</option>
          <option value="fadeOutDown">fadeOutDown</option>
          <option value="fadeOutDownBig">fadeOutDownBig</option>
          <option value="fadeOutLeft">fadeOutLeft</option>
          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
          <option value="fadeOutRight">fadeOutRight</option>
          <option value="fadeOutRightBig">fadeOutRightBig</option>
          <option value="fadeOutUp">fadeOutUp</option>
          <option value="fadeOutUpBig">fadeOutUpBig</option>
        </optgroup>

        <optgroup label="Flippers">
          <option value="flip">flip</option>
          <option value="flipInX">flipInX</option>
          <option value="flipInY">flipInY</option>
          <option value="flipOutX">flipOutX</option>
          <option value="flipOutY">flipOutY</option>
        </optgroup>

        <optgroup label="Lightspeed">
          <option value="lightSpeedIn">lightSpeedIn</option>
          <option value="lightSpeedOut">lightSpeedOut</option>
        </optgroup>

        <optgroup label="Rotating Entrances">
          <option value="rotateIn">rotateIn</option>
          <option value="rotateInDownLeft">rotateInDownLeft</option>
          <option value="rotateInDownRight">rotateInDownRight</option>
          <option value="rotateInUpLeft">rotateInUpLeft</option>
          <option value="rotateInUpRight">rotateInUpRight</option>
        </optgroup>

        <optgroup label="Rotating Exits">
          <option value="rotateOut">rotateOut</option>
          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
          <option value="rotateOutDownRight">rotateOutDownRight</option>
          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
          <option value="rotateOutUpRight">rotateOutUpRight</option>
        </optgroup>

        <optgroup label="Sliding Entrances">
          <option value="slideInUp">slideInUp</option>
          <option value="slideInDown">slideInDown</option>
          <option value="slideInLeft">slideInLeft</option>
          <option value="slideInRight">slideInRight</option>

        </optgroup>
        <optgroup label="Sliding Exits">
          <option value="slideOutUp">slideOutUp</option>
          <option value="slideOutDown">slideOutDown</option>
          <option value="slideOutLeft">slideOutLeft</option>
          <option value="slideOutRight">slideOutRight</option>
          
        </optgroup>
        
        <optgroup label="Zoom Entrances">
          <option value="zoomIn">zoomIn</option>
          <option value="zoomInDown">zoomInDown</option>
          <option value="zoomInLeft">zoomInLeft</option>
          <option value="zoomInRight">zoomInRight</option>
          <option value="zoomInUp">zoomInUp</option>
        </optgroup>
        
        <optgroup label="Zoom Exits">
          <option value="zoomOut">zoomOut</option>
          <option value="zoomOutDown">zoomOutDown</option>
          <option value="zoomOutLeft">zoomOutLeft</option>
          <option value="zoomOutRight">zoomOutRight</option>
          <option value="zoomOutUp">zoomOutUp</option>
        </optgroup>

        <optgroup label="Specials">
          <option value="hinge">hinge</option>
          <option value="rollIn">rollIn</option>
          <option value="rollOut">rollOut</option>
        </optgroup>
      </select>

      <button class="btn js--triggerAnimation">Animate it</button>
    </form>
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script>
  function testAnim(x) {
    $('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass();
    });
  };

  $(document).ready(function(){
    $('.js--triggerAnimation').click(function(e){
      e.preventDefault();
      var anim = $('.js--animations').val();
      testAnim(anim);
    });

    $('.js--animations').change(function(){
      var anim = $(this).val();
      testAnim(anim);
    });
  });

</script>
</div>
                <hr>
                <p>The Vampire Diaries is an American supernatural drama television series developed by Kevin Williamson and Julie Plec, 
				based on the popular book series of the same name written by L. J. Smith. The series premiered on The CW on September 10, 2009. </p>
               
            </div>
			<br/>
			<form action="index.php" method="get">
	
	<strong><font size="3">Search Engine:</font></strong><input type="text" id="txt1" name="value" placeholder="Search Anything here" onkeyup="showHint(this.value)">
	
	<input type="submit" name="search" value="Search Now">
	<p><font> Suggestions: <span id="txtHint"></span></font></p>
	</form>
	<hr>
	
	<?php
	mysql_connect("localhost","root","");
	mysql_select_db("users");
	
	if(isset($_GET['search'])) {
		$search_value = $_GET['value'];
	
		$query = "select * from sites where site_keywords like '%$search_value%'";
		
		$run = mysql_query($query);
		
		while($row=mysql_fetch_array($run)){
		
		$title = $row['site_title'];
		$link = $row['site_link'];
		$desc = $row['site_description'];
		
		echo "<font><a href='$link'><h1>$title</h1></a><p>$desc</p></font>";
		
	}
	}
	?>
        </div>
    </header>
	
    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">The Vampire Diaries (Novel Series)</h2>
                    <hr class="light">
					
                    <p class="text-faded" align="justify">The series takes place in Mystic Falls, Virginia, a fictional small town haunted and surrounded by supernatural beings. 
					The series narrative follows the protagonist Elena Gilbert (Nina Dobrev) as she falls in love with vampire Stefan Salvatore (Paul Wesley) 
					and is drawn into the supernatural world as a result. As the series progresses, Elena finds herself drawn to Stefan's brother Damon Salvatore 
					(Ian Somerhalder) resulting in a love triangle. As the narrative develops in the course of the series, the focal point shifts on the mysterious 
					past of the town involving Elena's malevolent doppelgänger Katerina Petrova. Katerina was the love of both Damon and Stefan Salvatore many years 
					ago. Her return,
					along with the family of Original Vampires, have all led to many plots against Elena and Mystic Falls.<p>
                    <div id="XML" align="center">
					<button class="btn btn-default btn-xl" onclick="loadXMLDoc('cd_catalog.xml')">Ajax & XML</button><br/><br/>	
					</div><br/><br/>
					<div>
					<button class="btn btn-default btn-xl"><a href="xml.php">Database & XML</a></button><br/><br/>
					</div>
					
					<div>
						<button class="btn btn-default btn-xl"><a href="dynamicjson.json">Dynamic Json</a></button>	<br/><br/>
					</div><br/><br/>
					
					<div>
						<button type="button" class="btn btn-default btn-xl"><a href="dynamicjson.php">Execute Json</a></button>	<br/><br/>
					</div><br/><br/>
					



					
<script>
var xmlhttp = new XMLHttpRequest();
var url = "dynamicjson.json";

xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        myFunction(xmlhttp.responseText);
    }
}
xmlhttp.open("GET", url, true);
xmlhttp.send();

function myFunction(response) {
    var arr = JSON.parse(response);
    var i;
    var out = "<table>";

    for(i = 0; i < arr.length; i++) {
        out += "<tr><td>" + 
        arr[i].Name +
        "</td><td>" +
        arr[i].City +
        "</td><td>" +
        arr[i].Country +
        "</td></tr>";
    }
    out += "</table>";
    document.getElementById("id01").innerHTML = out;
}
</script>
             
           
    </section>

    <section id="videos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tracklist</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            
                      <iframe src="https://player.vimeo.com/video/20255558" width="100%" height="400" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p>
            </div>
        </div>
    </section>

    <section class="no-padding" id="wallpapers">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/damon.jpg" data-lightbox="example-set" data-title="Damon Salvatore" class="portfolio-box">
                        <img src="img/design/damon.jpg" class="img-responsive" alt="">
						
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Damon Salvatore
                                </div>
                                <div class="project-name">
								<font size="2">
                                    Damon Salvatore: I just have to say something.</br>
									Elena Gilbert: Why do you have to say it with my necklace?</br>
									Damon Salvatore: Because what I'm about to say is... probably the most selfish thing I've ever said in my life.</br>
									Elena Gilbert: Damon. Don't go there.</br>
									Damon Salvatore: I just have to say it once. You just need to hear it. I love you Elena. And it's because I love you that... 
									I can't be selfish with you. And why you can't know this. I don't deserve you... but my brother does.</br>
									[Kisses her on her forehead]</br>
									Damon Salvatore: God, I wish you didn't have to forget this... but you do.</br>
									[Compells her]</br>
								</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/elena.jpg" data-lightbox="example-set" data-title="Elena Gilbert" class="portfolio-box">
                        <img src="img/design/elena.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Elena Gilbert
                                </div>
                                <div class="project-name">
								<font size="2">
                                    " Dear diary, I`m not a believer. People are born, they grow old and then they die. That`s the world we live in, 
									but how can I deny what`s right in front of me? Someone who never grows old, never gets hurt. Someone who changes in ways that can`t be explaine "
                                </font>
								</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/stefan.jpg" data-lightbox="example-set" data-title="Stefan Salvatore" class="portfolio-box">
                        <img height="100" src="img/design/stefan.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Stefan Salvatore
                                </div>
                                <div class="project-name">
									<font size="2">
                                    "You never knew Damon as a human. He was aimless, always searching for something more, sweet
									and earnest to a fault but never strong. That came later when he found himselft, when he 
									truly became Damon."
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/bonnie.jpg" data-lightbox="example-set" data-title="Bonnie Bennett" class="portfolio-box">
                        <img src="img/design/bonnie.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Bonnie Bennett	
                                </div>
                                <div class="project-name">
								<font size="2">
                                   " You`re mad at your little brother. You`ll yell at him a little, 
								   teach him a life lesson, you can`t be really mad like I`m mad. 
								   And you shouldn`t have to be. He`s your brother. "
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/caroline.jpg" data-lightbox="example-set" data-title="Caroline Forbes" class="portfolio-box">
                        <img src="img/design/caroline.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Caroline Forbes
                                </div>
								<div class="project-name">
								<font size="2">
                                   " I`m going to take you home. You`re going to forget that I`m a vampire. You`ll remember you got sick with the flue. 
								   You had a fever, chills and ickiness, but I made you soup. It was really salty. 
								   We bickered. You got better, and then your selfish little d "
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/design/jeremy.jpg" data-lightbox="example-set" data-title="Jeremy Gilbert" class="portfolio-box">
                        <img src="img/design/jeremy.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Jeremy Gilbert
                                </div>
                                <div class="project-name">
								<font size="2">
                                    " Jeremy: Haven’t you done enough? And how did you get in here? Alaric: I don’t know. Just wanted to say goodbye and it kind of happened. Jeremy: Ric? Alaric: I just want you to know that I’ll always be here to look after you, Jeremy. 
									That you’ll never be alone. Okay? I promise. Jeremy: I don’t understand. Oh my God.
									You’re a ghost. But if you’re dead that means Elena… "
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
				<div class="col-lg-4 col-sm-6">
                    <a href="img/design/alaric.jpg" data-lightbox="example-set" data-title="Alaric Saltzman" class="portfolio-box">
                        <img src="img/design/alaric.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                     Alaric Saltzman
                                </div>
                                <div class="project-name">
								<font size="2">
                                   "Neither one of us should be here right now. We spent our lives dodging fate and beating the odds.
									But because we did I got to meet you, the most beautiful, hilarious, and intimidatingly brilliant woman 
									I have ever known. You inspire me. You've shown me that happiness is something I can actually have in my 
									life. And so, I promise to be with you and love you and to dodge fate with you for the rest of our lives."
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
				<div class="col-lg-4 col-sm-6">
                    <a href="img/design/matt.jpg" data-lightbox="example-set" data-title="Matt Donovan" class="portfolio-box">
                        <img src="img/design/matt.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                     Matt Donovan 
                                </div>
                                <div class="project-name">
								<font size="2">
                                    Matt: Looking for someone?</br>

									Elena: Hey.</br>

									Matt: When you broke up with me, you said it was because you needed some time alone. You don't look so alone to me.</br>

									Elena: Matt, you don't understand. It's--</br>

									Matt: That's okay, Elena. You do what you have to do. I just want to let you know that. . .I still believe in us. And I'm not giving up on that.</br>

									Elena: Matt. . .</br>
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
				<div class="col-lg-4 col-sm-6">
                    <a href="img/design/tylar.jpg" data-lightbox="example-set" data-title="Tyler Lockwood" class="portfolio-box">
                        <img src="img/design/tylar.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Tyler Lockwood
                                </div>
                                <div class="project-name">
								<font size="2">
                                    " Tyler: The bond isn't broken until he doesn't feel the pain of transformation anymore. 
									If we wanna get back at Klaus for everything he's done to us, Adrian has to keep turning.
									Girl: He doesn't have to do anything. Isn't that the point of breaking the sire bond, free will? "
									</font>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>


	<script>
var myCenter=new google.maps.LatLng(53.8036425,-1.5474292);
var marker;

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("Leeds"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  animation:google.maps.Animation.BOUNCE
  
  });

marker.setMap(map);
var infowindow = new google.maps.InfoWindow({
  content:"Leeds Beckett University"
  });

infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>



<script>
var myCenter1=new google.maps.LatLng(2.822796,101.872789);
var marker;

function initialize()
{
var mapProp = {
  center:myCenter1,
  zoom:5,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("Legenda"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter1,
  animation:google.maps.Animation.BOUNCE
  });

marker.setMap(map);
var infowindow = new google.maps.InfoWindow({
  content:"Legenda University College "
  });

infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
				
                    <h2 class="section-heading">Contact Us</h2>
                    <hr class="primary"> 
                    <b><table><tr><td>
					<div align="left-side" id="Legenda" style="width:500px;height:380px;"></div>
					</td><td>
					<b>Legenda University College </br></br>
					Persiaran UTL, Batu 12,</br>
					&nbsp; &nbsp;&nbsp;&nbsp;  71700, Mantin, Negeri Sembilan,</br>
					Malaysia.</br>
					<a href="mailto:info@leg.edu.my">info@leg.edu.my</a></br>
					(606) 758 7888</br>
					(606) 758 7599</br></b></td></tr></table>
					
					</br>
					 <table><tr><td>
					<div align="right-side" id="Leeds" style="width:500px;height:380px;"></div>
					</td><td>&nbsp; &nbsp;&nbsp;&nbsp;Leeds Beckett University</br></br>
					Student Hub</br>
					0113 812 3000</br>
					&nbsp; &nbsp;&nbsp;&nbsp;<a href="mailto:studenthub@leedsbeckett.ac.uk">studenthub@leedsbeckett.ac.uk</a></br></td></tr></table>
                </div>
              
                 
               
               
            </div>
        </div>
    </section>


    <script src="js/jquery.js"></script>

	<script src="js/lightbox-plus-jquery.min.js"></script>
    <script src="js/design.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/creative.js"></script>

</body>

</html>
