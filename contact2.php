<?php 
$errors = '';
$myemail = 'email@casferchang.com';//<-----Put Your email address here.
if(empty($_POST['name1'])  || 
   empty($_POST['email1']) || 
   empty($_POST['message1']))
{
    $errors .= "\n Error: all fields are required";
}

$name = $_POST['name1']; 
$email_address = $_POST['email1']; 
$message = $_POST['message1']; 

if (!preg_match(
"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", 
$email_address))
{
    $errors .= "\n Error: Invalid email address";
}

if( empty($errors))
{
	$to = $myemail; 
	$email_subject = "Contact form submission: $name";
	$email_body = "You have received a new message. ".
	" Here are the details:\n Name: $name \n Email: $email_address \n Message \n $message"; 
	
	$headers = "From: $myemail\n"; 
	$headers .= "Reply-To: $email_address";
	
	mail($to,$email_subject,$email_body,$headers);
	//redirect to the 'thank you' page
	echo "<script type='text/javascript'>alert('Thank you, we'll reply you as soon as possible')</script>";
	
} 
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>


<script>
	
function val2()
{
  var nam=/^[a-zA-Z]{8,27}$/;
    var pas=/^[a-zA-Z0-9-_]{6,16}$/;
	   var ema=/^[a-zA-Z0-9-_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,3}$/;
	
	 
	  
   if(document.validation.name.value.search(nam)==-1)
    {
	 alert("Please fill in your name");
	 document.validation.name.focus();
	 return false;
	 }
	
  
   else if(document.validation.password.value.search(pas)==-1)
    {
	 alert("Please fill in your password");
	 document.validation.password.focus();
	 return false;
	 }
	 
	 
	 else if(document.validation.email.value.search(ema)==-1)
    {
	 alert("Please fill in your email address");
	 document.validation.email.focus();
	 return false;
	 }
	 

	 else 
	{
	 return true;
	 }
	 }
	
	 
</script>


<body>

<body>


<?php


//define variables and set to empty values
$errName = $errPass = $errEmail = "";
$name = $password = $email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty($_POST["name1"])) {
		$errName = "Name is required";
	} else {
		$name = test_input($_POST["name1"]);
		//check if name only contains letters and whitespace
		if (!preg_match("/^[a-zA-Z]*S/",$name)) {
			$errName = "Only letters and white space allowed";
		}
	}

	$msg = wordwrap("ABC",100);
	
			
	if (empty($_POST["email1"])) {
		$errEmail = "Email is required";
	} else {
		$email = test_input($_POST["email1"]);
		
		//check if e-mail  address is well-formed
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errEmail = "Invalid email format";
		}
		else{
			mail($email,"My subject",$msg);
		}
	}
	
	if (empty($_POST["message1"])) {
		$messaage = "";
	} else {
		$message = test_input($_POST["message1"]);
	}
	
	
}

?>
  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="contact2.php"> AIDA~~~</a></h1>
		<a class="right" href="logout.php">Log Out</a>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
        <li><a href="index2.php">Home</a></li>
          <li><a href="oot2.php">OO Testing</a></li>
          <li><a href="about2.html">About Us</a></li>
          <li class="current"><a href="contact2.php">Contact Us</a></li>
       </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        <!-- insert your sidebar items here -->
        <h3>Contact Us</h3>
		<h4 class="center">Object Oriented PHP AIDA</h4>
        <p class="center">casferchang92@gmail.com</p>
		<p class="center">012 3456789</p>
		<p class="center">+603 2163 3689</p>
		
		</div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Contact Us</h1>
        <p>Fill in the detail and deliver message to us and we'll reply you as soon as possible.</p>
      
<p><span class="error">* required field.</span></p>
<form id="myForm" action="contact2.php" method="post" action="">
	Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="name2" name="name1" required>
	<span class="error">*<?php echo $errName;?></span>
	<br><br>
	
	Email: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="email2" name="email1" required>
	<span class="error">*<?php echo $errEmail;?></span>
	<br><br>
	Message: <textarea id="message2" name="message1" rows="5" cols="40" required></textarea>
	
	<br><br>

	<span class="right"><input type="submit" name="submit" value="Submit"></span>
</form>
        <p><br /><br />*Please enter some useful comments or important questions.</p>
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>

