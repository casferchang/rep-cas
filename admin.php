
<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>


  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="index.php"> AIDA~~~</a></h1>
		
      </div>
      <div id="menubar">
	  
        <ul id="menu">
          <li class="current"><a href="admin.php">Admin</a></li>
          <li><a href="logout.php">Log Out</a></li>
          
		 
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        
        <div class="sidebar">
        
        <p>
			<form method="post"   action="create.php">

				<fieldset class="fieldset-width1">
					<legend>
						Enter New Product Details
					</legend>
					<label class="align"for="txtId">Id: </label><br />
					<input type="text" name="id" required/>
					<br />
					<br />

					<label class="align"for="txtCreator">Title: <br /> </label>
					<input type="text" name="title" required/>
					<br />
					<br />
					<label class="align"for="txtCreator">Creator: <br /></label>
					<input type="text" name="creator"  required/>
					<br />
					<br />
					<label class="align" for="txtPrice">Price: <br /></label>
					<input type="text" name="price"  required/>
					<br />
					<br />
					<label class="align" for="txtDesc">Description: <br /></label>
					<input type="text" name="desc"  required/>
					<br />
					<br />
					<label class="align" for="txtImage">Image Filename: </label>
					<input type="file" name="img"  required/>
					<br />
					<br />

					<input type="submit" value="Submit" name='submit' />
					<input type="reset" value="Clear" />
			</form>
		</p>
		</div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Admin</h1>
     
<?php

	include "config/config.php";
	
	$result = "";
	$mysqli_conn = new mysqli ($db['hostname'], $db ['username'], 
							$db['password'], $db ['database']);

	if ($_SERVER ["REQUEST_METHOD"] == "GET") {
		
		
		$result =$mysqli_conn ->query ("SELECT * FROM products");
		
			
		echo '<table><tr><th>Title</th><th>Creator</th><th>Price</th><th>Description</th><th>Image</th><th>Update</th><th>Delete</th></tr>';
		while ($row = mysqli_fetch_assoc($result)) {
			echo '<tr>';
			echo '<td>' . $row['title'] . '</td>';
			echo '<td>' . $row['creator'] . '</td>';
			echo '<td>' . $row['price'] . '</td>';
			echo '<td>' . $row['desc'] . '</td>';
			echo '<td>' . $row['img'].'</td>';
			echo '<td><a href="AmendProduct.php?id=' . $row['id'] . '">Update</a></td>';
			echo '<td><a href="DeleteProduct.php?id=' . $row['id'] . '">Delete</a></td>';
			echo '</tr>';
		}
		echo '</table>';
			
		}
	
	
	?>
     
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>

</body>
</html>
