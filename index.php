<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
<?php

include "config/config.php";
$errInput = "";

$result = "";
$errName = $errPass = "";
$userName = $userPassword = "";

$mysqli_conn = new mysqli($db['hostname'],$db['username'],$db['password'],$db['database']);
	
	if ($mysqli_conn -> connect_errno) {//check the connection 
		print "Failed to connect to MySQL: (".$mysqli_conn -> connect_errno .")".$mysqli_conn -> connect_error;
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST["userName"])) {
			$errName = "Name is required";
		}
		else {
			$userName = test_input($_POST["userName"]);
			
			if (empty($_POST["userPassword"])) {
				$errPass = "Password is required";
			}
			else {
				$userPassword = test_input($_POST["userPassword"]);
				
				
				$adminlog = $mysqli_conn->query("SELECT * FROM admin WHERE adminName = '".$userName."' and adminPassword = '".$userPassword."'");
				
				$result = $mysqli_conn->query("SELECT * FROM users WHERE userName = '".$userName."' and userPassword = '".$userPassword."'");
				
				if($row = $result->fetch_assoc()) {
					session_start();
					$_SESSION["login_user"] = $userName;
					echo "BBB"." ".$_SESSION['login_user'];
					echo $row['userName'];
					echo "<script type='text/javascript'>alert('Hi,".$userName."')</script>";
					header("Location: index2.php");
					
					
				} else 
				if ($row = $adminlog->fetch_assoc()) {
					session_start();
	
					$_SESSION['login_admin'] = $userName;
					header("Location: admin.php");
				}
				else 
				{
					echo "<script type='text/javascript'>alert('Please use the correct name and password')</script>";
				}
			}
		}
	}
	
	function test_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}


?>

  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="index.php"> AIDA~~~</a></h1>
		
      </div>
      <div id="menubar">
	  
        <ul id="menu">
          <li class="current"><a href="index.php">Home</a></li>
         <li><a href="register.php">Register</a></li>
          <li><a href="oot.html">OO Testing</a></li>
          <li><a href="about.html">About Us</a></li>
          <li><a href="contact.php">Contact Us</a></li>
		 
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        
        <h3>Login</h3>
         <p>Please login if you have an account to access to the functions.</p>
		   <form method="post" 	 id="subscribe">
            Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="name3" name="userName" onChange="return nam()"required>
			
			<br><br>
			Password: &nbsp;<input type="password" id="password3" name="userPassword" onChange="return pas()"required>
			
			<br><br>
			<span class="right"><input type="submit" name="submit" value="Submit"></span>
          </form>
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Register</h3>
		  <p>Don't have an account?<a href="register.php"> Register</a> an account for yourself now.</p>
         
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Notice</h3>
          <p>You can test your PHP skills <a href="https://www.codecademy.com/courses/web-beginner-en-bH5s3/0/1">here</a>.</p>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>PHP Introduction</h1>
     

		<p>PHP began its OOP life with PHP4, and has a pretty decent object model with PHP5. The release of PHP6 - who knows when? - 
		is destined to complete the object model nicely. </p>
		<p>The huge debate about whether OOP is better than procedural or not still rages on, 
		with no end in sight. Whether this debate will ever be concluded is another story altogether, but I strongly believe 
		that object oriented development is a better way to go. Perhaps this series of articles will show why. </p>
		<p>In this class we will explore the essential principles of object oriented programming (OOP) with PHP. Our goal is to
		 define what object oriented programming is, and what the advantages and disadvantages of using it are.</p>
        <p>In PHP we can define objects in similar ways; we can assign properties to them, as well as make them do things programmatically. 
		Obviously these objects will only exist in the program itself, but through user interfaces such as web pages, we can interact with 
		them and make them perform tasks and procedures, as well as calculate, retrieve and modify properties.
		<p>PHP gives us a very simple way to define an object programmatically, and this is called a class. A class is a wrapper that defines and 
		encapsulates the object along with all of its methods and properties. You can think of a class as a programmatic representation of an object,
		and it is the interface that PHP has given you, the developer, to interact with and modify the object. </p>
		
		
        <h2>Online Functions</h2>
		<ul>
          <p>You can only access to the shop after login. If you don't have an account you can register instantly.</p>
        </ul>
     
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>
