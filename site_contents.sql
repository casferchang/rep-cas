-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2015 at 04:36 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site_contents`
--

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE IF NOT EXISTS `site_contents` (
  `id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `content` text NOT NULL,
  `published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`id`, `title`, `content`, `published`) VALUES
(1, 'Andrea', 'A gorgeous lady that one of a kind, the most rare female on the face of the earth; the kind that every guy dreams about. She is confident, honest, loyal, protective of what she has, strong, artistic, beautiful inside and out, exellent lover. A tower of strength for those she cares for, the rock to her family. She is a person you can depend on, just don''t go too far in crossing lines with her or those she loves. Or else you better run and get out of her way...because she will come out and get you.', '2013-02-11 18:30:00'),
(2, 'Protohype', 'The process of leaking a prototype device to generate buzz about a product you don''t quite yet have ready for market to a friendly tech website who will promote the gizmo well before it''s ready to go.\r\nGizmodo''s finding of that Apple iPhone 4G months before it''s released is clearly a case of protohype.', '2013-04-09 18:30:00'),
(3, 'Shrooms', 'something that God put on this world to open up a mental gateway and help us better understand what the real purpose and meaning of life is, but is used by few due to the fact that the fuckin government makes it out to be a bad thing and therefore illegalized it\r\nadam and eve probably first discovered God after eating the shrooms he conveniently placed in the garden. what the heck does that tell you?', '2013-06-10 18:30:00'),
(4, 'Urkin', 'Term developed by The Sports Guy Bill Simmons''s friend House. Used to describe multiple sports programs being at commercial simultaneously, as CBS frequently does during their NCAA tourney coverage. The dreaded quadraple urkin is when all four games being covered are at commercial at once. \r\n', '2013-06-15 18:30:00'),
(5, 'Ratchet', 'A diva, mostly from urban cities and ghettos, that has reason to believe she is every mans eye candy. Unfortunately, she''s wrong.\r\n\r\nTypical signs to beware of include, but are not limited to: \r\n-owning a Blackberry \r\n-BLARES anything by Drake, 2Chainz, Nicki Minaj, Gucci Mane, Waka Flocka, Lil Wayne, T-Pain, Cali Swag District, or any other garbage entertainment rapper \r\n-rowdily quotes "lyrics" from aforementioned artists ', '2014-06-17 18:30:00'),
(6, 'Quinn Fabray', 'A character on Glee who is a girl despite her strangely chosen name. She''s a cheerleader and HBIC at McKinley High. She''s always involved in some sort of scandal such as getting pregnant while being head of the celibacy club as she is actually batshit crazy in reality. Picks on Rachel Berry relentlessly for no apparent reason, except maybe the fact that she is secretly in love with her.', '2013-07-31 18:30:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `site_contents`
--
ALTER TABLE `site_contents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `site_contents`
--
ALTER TABLE `site_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
