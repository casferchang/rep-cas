<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="contact2.php"> AIDA~~~</a></h1>
		<a class="right" href="logout.php">Log Out</a>
		<a class="right" href="index2.php">Back</a>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li><a href="register2.php">Account</a></li>
          <li><a href="oot2.php">OO Testing</a></li>
          <li><a href="about2.html">About Us</a></li>
          <li><a href="contact2.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        <!-- insert your sidebar items here -->
        <h3>Upload Image</h3>
		</br>
		<p>Choose a image</p>
		<p>Select a function</p>
		<p>Upload. </p>
		
		</div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Programming PHP Shop</h1>
		<form enctype="multipart/form-data" method="post" action="upload.php">
            <label>Choose a file to upload <input type="file" size="32" name="image_field" value="" /></label><br/>
            <h4>Operations</h4>
            <label><input type="radio" name="operation" value="resize" /> Resize Image(100x100)</label><br/>
            <label><input type="radio" name="operation" value="greyscale" /> Make Greyscale Image</label><br/>
            <label><input type="radio" name="operation" value="watermark" /> Add image watermark</label><br/>
            <label><input type="radio" name="operation" value="reflection" /> Create reflection</label><br/>
            <label><input type="radio" name="operation" value="text" /> Text on Image</label><br/><br/>
            <input type="submit" name="Submit" value="upload" />
        </form>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	include_once 'include/class.upload.php';

    $handle = new upload($_FILES['image_field']);

    if ($handle->uploaded) {
    	if (!isset($_POST['operation'])) {
	    	// Include global options
	    	include 'include/options.php';

	        echo '<h3>Image Info:-</h3>';
	        echo '<ul>';
	        echo '<li> Image Source Name: ' . $handle->file_src_name . '</li>';
	        echo '<li> Image size: ' . $handle->file_src_size . ' bytes</li>';
	        echo '<li> Width: ' . $handle->image_src_x . '</li>';
	        echo '<li> Height: ' . $handle->image_src_y . '</li>';
	        echo '<li> Image Type: ' . $handle->image_src_type . '</li>';   
	        echo '</ul>';

	        $handle->process('uploads/');

	        if ($handle->processed) {
	        	echo '<h3>Original Uploaded file(resized by default to 960x540px)</h3>';
	            echo "<div><img style='border: 1px solid #333;' src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo '<p>Error :<br/>' . $handle->error . '</p>';
	        }
	    }

        if (isset($_POST['operation']) && $_POST['operation'] === 'resize') {
        	include 'include/options.php';
        	// Resize the image
			$handle->image_resize = true;
			$handle->image_x = '100';
			$handle->image_y = '100';
			$handle->file_name_body_pre = 'resize_';
			$handle->process('uploads/');
	        if ($handle->processed) {
	            echo '<h4>Resized Image</h4>';
	            echo "<div><img style='border: 1px solid #333;' src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo 'error :<br/>' . $handle->error;
	        }
        }
        
        // Make image greyscale
        if (isset($_POST['operation']) && $_POST['operation'] === 'greyscale') {
        	include 'include/options.php';
        	$handle->file_name_body_pre = 'greyscale_';
        	// Make a greyscale image
	        $handle->image_greyscale = true;
	        $handle->process('uploads/');
	        if ($handle->processed) {
	            echo '<h4>Greyscale image created<h4/>';
	            echo "<div><img style='border: 1px solid #333;' src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo 'error :<br/>' . $handle->error;
	        }
	    }

	    // Watermark image
        if (isset($_POST['operation']) && $_POST['operation'] === 'watermark') {
        	include 'include/options.php';
        	$handle->file_name_body_pre = 'watermark_';
        	// Watermark image
	        $handle->image_watermark = 'img/codedodle.png';
	        $handle->process('uploads/');
	        if ($handle->processed) {
	            echo '<h4>Watermarked Image<h4/>';
	            echo "<div><img style='border: 1px solid #333;' src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo 'error :<br/>' . $handle->error;
	        }
	    }

	    // Image reflection
        if (isset($_POST['operation']) && $_POST['operation'] === 'reflection') {
        	include 'include/options.php';
        	$handle->file_name_body_pre = 'reflection_';
        	// Reflect image
	        $handle->image_reflection_height = '20%';
	        $handle->image_reflection_space = 0;
	        $handle->process('uploads/');
	        if ($handle->processed) {
	            echo '<h4>Reflected Image<h4/>';
	            echo "<div><img src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo 'error :<br/>' . $handle->error;
	        }
	    }

	    // Text on image
        if (isset($_POST['operation']) && $_POST['operation'] === 'text') {
        	include 'include/options.php';
        	$handle->file_name_body_pre = 'text_';
        	// Text on image
	        $handle->image_text = 'CodeDodle';
	        $handle->image_text_color = '#3b88c3';
	        $handle->image_text_opacity = 70;
	        $handle->image_text_position = 'TR';
	        $handle->process('uploads/');
	        if ($handle->processed) {
	            echo '<h4>Text on Top Right Corner<h4/>';
	            echo "<div><img src='$handle->file_dst_pathname'/></div>";
	        } else {
	            echo 'error :<br/>' . $handle->error;
	        }
	    }
	    // Remove the uploaded image.
	    $handle->clean();
    }
}
?>
 
        
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>


