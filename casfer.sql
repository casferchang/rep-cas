-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2015 at 04:35 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casfer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `adminName` varchar(20) NOT NULL,
  `adminPassword` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminName`, `adminPassword`) VALUES
('admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `creator` varchar(255) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `desc` longtext NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `creator`, `price`, `desc`, `img`) VALUES
(1, 'HTML & CSS', 'Jon Duckett', '152.00', 'HTML & CSS: Design and Build Web Sites', 'backgrounds/1.jpg'),
(2, 'Python Programming', 'MikeDawson', '119.00', 'Python Programming for the Absolute Beginner', 'backgrounds/2.jpg'),
(3, 'Web Design', 'Jon Duckett\r\n', '136.00', 'Web Design with HTML, CSS, JavaScript and jQuery Set', 'backgrounds/3.jpg'),
(4, 'JS & Jquert', 'Jon Duckett', '108.00', 'JavaScript & JQuery: Interactive Front-end Web Development', 'backgrounds/4.jpg'),
(5, 'JavaScript', 'Douglas Crockford', '79.00', 'JavaScript: The Good Parts', 'backgrounds/5.jpg'),
(6, 'Object Oriented Programming with C++', 'Balagurusamy', '158.00', 'Object Oriented Programming with C++ (English) 6th Edition (Paperback)', 'backgrounds/6.jpeg'),
(7, 'Web Technologies', 'Solutions, Kogent Learning', '210.90', 'Web Technologies HTML, Javascript, PHP, Java, JSP, ASP.NET, XML and AJAX Black Book (With CD) (English) (Paperback)', 'backgrounds/7.jpeg'),
(8, 'Programming PHP', 'Kevin Tatroe  (Author), Peter MacIntyre (Author), Rasmus Lerdorf (Author)', '79.90', 'Programming PHP Third Edition Edition', 'backgrounds/8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userName` varchar(50) NOT NULL,
  `userPassword` varchar(30) NOT NULL,
  `userEmail` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userName`, `userPassword`, `userEmail`) VALUES
('asd', 'asd', 'asd@gmail.com'),
('casferchang', 'casfer123', 'casferchang92@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
