<!DOCTYPE HTML>
<html>
 <head>
  <title>Object Oriented PHP</title>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="script.js"></script>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<script>
function nam()
{
  var nam=/^[a-zA-Z]{8,27}$/;
   if(document.validation.userName.value.search(nam)==-1)
    {
	 alert("Please enter a valid name");
	 document.validation.userName.focus();
	 return false;
	 }
	} 
	 
		
function pas()
	{
	var pas=/^[a-zA-Z0-9-_]{6,16}$/;
   if(document.validation.userPassword.value.search(pas)==-1)
    {
	 alert("Please enter a valid password");
	 document.validation.userPassword.focus();
	 return false;
	 }
	 }
	 
	 
function ema()
{
 var ema=/^[a-zA-Z0-9-_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,3}$/;
   if(document.validation.userEmail.value.search(ema)==-1)
    {
	 alert("Please enter a valid email");
	 document.validation.userEmail.focus();
	 return false;
	 }
	} 
	 
	
function val()
{
  var nam=/^[a-zA-Z]{8,27}$/;
    var pas=/^[a-zA-Z0-9-_]{6,16}$/;
	   var ema=/^[a-zA-Z0-9-_\.]+@[a-zA-Z]+\.[a-zA-Z]{2,3}$/;
	
	 
	  
   if(document.validation.userName.value.search(nam)==-1)
    {
	 alert("Please fill in your name");
	 document.validation.userName.focus();
	 return false;
	 }
	
  
   else if(document.validation.userPassword.value.search(pas)==-1)
    {
	 alert("Please fill in your password");
	 document.validation.userPassword.focus();
	 return false;
	 }
	 
	 
	 else if(document.validation.userEmail.value.search(ema)==-1)
    {
	 alert("Please fill in your email address");
	 document.validation.userEmail.focus();
	 return false;
	 }
	 

	 else 
	{
	 return true;
	 }
	 }
	
	 
</script>

<body>

<?php

include "config/config.php";



$mysqli_conn = new mysqli($db['hostname'],$db['username'],$db['password'],$db['database']);
//define variables and set to empty values

	if ($mysqli_conn -> connect_errno) {//check the connection 
		print "Failed to connect to MySQL: (".$mysqli_conn -> connect_errno .")".$mysqli_conn -> connect_error;
	}
	
	
$errName = $errPass = $errEmail = "";
$userName = $password = $email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty($_POST["userName"])) {
		$errName = "Name is required";
	} else {
		$userName = test_input($_POST["userName"]);
		//check if name only contains letters and whitespace
		if (!preg_match("/^[a-zA-Z]*S/",$userName)) {
			$errName = "Only letters and white space allowed";
		} 
	
	if (empty($_POST["userPassword"])) {
				$passwordErr = "Password is required";
			} else {
				$userPassword = test_input($_POST["userPassword"]);
 
	if (empty($_POST["userEmail"])) {
		$errEmail = "Email is required";
	} else {
		$userEmail = test_input($_POST["userEmail"]);
		$msg = wordwrap("ABC",100);
		//check if e-mail  address is well-formed
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errEmail = "Invalid email format";
		}
		else{
			mail($email,"My subject",$msg);
		}
	if (empty($_POST["captcha"])) {
		$errCaptcha = "Captcha is required";
	} 
	}
	}	
				
	
		$userRepeat = $mysqli_conn->query("SELECT * FROM users WHERE userName = '".$userName."'");
		$emailRepeat = $mysqli_conn->query("SELECT * FROM users WHERE userEmail = '".$userEmail."'");
		
		if ($row = $userRepeat->fetch_assoc()) {
			
			echo "<script type='text/javascript'>alert('Sorry, the name has been used by other people. Please try other name')</script>";
		} 
		else  
		if ($row = $emailRepeat->fetch_assoc()) {
			
			echo "<script type='text/javascript'>alert('Sorry, the email has been used by other people. Please try other email')</script>";
		
		}
		else {
			$query_str = "INSERT INTO users VALUES ('".$userName."','".$userPassword."','".$userEmail."')";
			$mysqli_conn -> query($query_str);
			
			echo "<script type='text/javascript'>alert('Register successfully, thank you!')</script>";
			
		}
		
			}
			
	}		

	
function test_input($data) {
	$data = trim($data);
	$data = stripslashes ($data);
	$data = htmlspecialchars($data);
	return $data;
}
//} else
//{
	//echo "<script type='text/javascript'>alert('Please fill in correct captcha')";
//}
?>



  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="register.php"> AIDA~~~</a></h1>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.php">Home</a></li>
          <li class="current"><a href="register.php">Register</a></li>
          <li><a href="oot.html">OO Testing</a></li>
          <li><a href="about.html">About Us</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
	    <h2>Filling Informations</h2>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        <h3>Name</h3>
        <p>Name must be from at least <strong>8</strong> until <strong>27</strong></p>
		<p>Name cannot involve any <strong>numeric</strong> character or <strong>symbol</strong>.</p>
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
		<h3>Password</h3>
        <p>Password must be from at least <strong>6</strong> until <strong> 16 </strong>.</p>
		<p>Password cannot involve any <strong>symbol</strong>.</p>
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        <h3>Email</h3>
        <p>Email must be include <strong>@</strong> as a valid email.</p>
		<p>Verification will be sent to your email.</p>

        </div>
      </div>
      <div id="content">
       <h1>Registration</h1>
		<p><span class="error">* required field.</span></p>
		<form method="post" name="validation" onSubmit="return val()">
			Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<input type="text" id="name1" name="userName" onChange="return nam()"required>
			<span class="error">*<?php echo $errName;?></span>
			<br><br>
			Password: &nbsp;
			<input type="password" id="password1" name="userPassword" onChange="return pas()"required>
			<span class="error">*<?php echo $errPass;?></span>
			<br><br>
			Email: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" id="email1" name="userEmail" onChange="return ema()"required>
			<span class="error">*<?php echo $errEmail;?></span>
			<br><br>
			
			Captcha: &nbsp;&nbsp;
			<input id="captcha1" name="captcha" type="text" required> *
			<br><br>
			<table><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>
			<div id="imgdiv"><img id="img" src="captcha.php" /></div></td><td><img id="reload" src="reload.png" /></td></tr>
			<tr><td></td><td><input type='button' id="button" value='Verify'></td></tr></table>
			
			
			<span class="center"><input type="submit" name="submit" value="Submit"></span>
			
			
			
		</form>
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>
