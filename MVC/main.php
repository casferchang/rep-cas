<?php 
require_once('user_model.php');

session_start();
if(!isset($_SESSION['user'])) {
header("Location:login.php");
}
else {
	$user = $_SESSION['user'];
}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>

  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Model View Controller<a href="main.php"> MVC Framework~~~</a></h1>

      </div>
      <div id="menubar">
	  
        <ul id="menu">
          <li class="current"><a href="main.php">Home</a></li>
        
		  	 
        </ul>
		
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        
        <h3>Welcome</h3>
         <p>Welcome to the PHP learning website</p>
		 <p>You may start access to the PHP example and learning</p>
		   <form method="post" action="#" id="subscribe">
           
			</form>
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Learning Output</h3>
		  <p>By learning PHP, you may know how to develop a wonderful website.</p>
		  <p>To create a website, several language need to realize immediately.</p>
		  
         
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Notice</h3>
          <p>You can test your PHP skills <a href="https://www.codecademy.com/courses/web-beginner-en-bH5s3/0/1">here</a>.</p>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
		<p>
          You are now logged in <?php print $user->get_username() ?>
		  This is MVC main page. 
		  <a href="index.php?op=logout">Logout</a>
		  </p>
        
		<ul>
		<h1>MVC Views</h1>
  <?php   require_once('connection.php');

  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
    $controller = 'pages';
    $action     = 'home';
  }

  require_once('views/layout.php');

		?>
        
        </ul>
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>