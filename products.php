<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="contact2.php"> AIDA~~~</a></h1>
		<a class="right" href="logout.php">Log Out</a>
		<a class="right" href="index2.php">Back</a>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li><a href="register2.php">Account</a></li>
          <li><a href="oot2.php">OO Testing</a></li>
          <li><a href="about2.html">About Us</a></li>
          <li><a href="contact2.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        <!-- insert your sidebar items here -->
        <h3>Purchase Item</h3>
		<p>Pick item and add to cart to display total price.</p>
		
		</div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Programming PHP Shop</h1>
		<?php
		session_start();
		require 'functions.php';
		$cart = new shoppingCart();
		$database = new Database_connect();
		$sql = 'select * from products order by title ASC';
		$result = mysql_query($sql);
		$output[] = '<ul>';
		while ($user_data = mysql_fetch_array($result)) {
			?>
			<table style="margin:110 auto;" width="auto" height="auto" border="1">
				<tr>
				<td>
				<img src="<?php echo $user_data['img']; ?>" width="350" height="350" style="padding:15px;">
				<h3><?php echo $user_data['title']; ?></h3> By <strong><?php echo $user_data['creator']; ?></strong>
				<h4>Description : <?php echo $user_data['desc']; ?></h4>
				<strong>Price : <?php echo $user_data['price'];?></strong>
				<a href="cart.php?action=add&id=<?php echo $user_data['id']; ?>">Add To Cart</a>
			</td>
			</tr>
			</table>
			<?php
}
$output[] = '</ul>';
echo join('',$output);
?>
        
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>


