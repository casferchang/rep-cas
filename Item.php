<?php

class Item {

  private $id;
  private $title;
  private $creator;
  private $price;
  private $desc;
  private $img;
 
  public function setId($id) {
       $this -> id = $id;
  }
  public function setTitle($title) {
       $this -> title = $title;
  }
  public function setCreator($creator) {
       $this -> creator = $creator;
  }
  
  public function setPrice($price) {
       $this -> price = $price;
  }
  
  public function setDesc($desc) {
       $this -> desc = $desc;
  }
  
  public function setImg($img) {
       $this -> img = $img;
  }
  
    public function __construct($id, $title){
	   $this->id = $id;
	   $this->title = $title;
	   
  }
  
  
   public function getId() {
      return $this -> id;
  }  
  
  public function getTitle() {
      return $this -> title;
  }
  
  public function getCreator() {
      return $this -> creator;
  }
  
  public function getPrice() {
       return $this -> price;
  }
  
  public function getDesc() {
       return $this -> desc;
  }
  
  public function getImg() {
       return $this -> img;
  }
  public function __toString() {
       return $this->id." ".$this->title." ".$this->creator." ".$this->price." ".$this->desc." ".$this->img;
  }

//Rest of class code
}
?>