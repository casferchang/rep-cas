-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2015 at 04:36 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `site_id` int(10) NOT NULL,
  `site_title` varchar(100) NOT NULL,
  `site_keywords` text NOT NULL,
  `site_link` text NOT NULL,
  `site_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_title`, `site_keywords`, `site_link`, `site_description`) VALUES
(1, 'Damon Salvatore\r\n', 'damon\r\nsalvatore\r\nDamon\r\nSalvatore\r\ndemon\r\nvampire\r\nsecret\r\nhunting', 'img/design/damon.jpg', 'Damon Salvatore is Stefan Salvatore''s older brother. It is implied that Stefan and Damon have been alive for more than 145 years, and that there was a love triangle between Damon, Stefan, and Katherine. Katherine was in love with both of them and before she left town (and faked her death) she turned them (fed them her blood before they died), but she still couldn''t decide who she wanted to be with. But later (when she comes back) we find out that she always loved Stefan. Damon is cynical and likes to antagonize his brother. It''s also stated that he and his brother haven''t seen each other in 15 years and Damon saying somewhere along the lines "Nice haircut. The 90''s grunge wasn''t looking good for you." When Damon is back everything gets complicated. Stefan finds new love - Elena, who looks exactly like Katherine. That''s why when Damon sees Elena for the first time, he falls in love with her too. Elena loves Stefan, but cares about Damon too.'),
(2, 'Elena Gilbert', 'elena\r\nElena \r\ngilbert\r\nGilbert\r\ngirl\r\nGirl\r\n\r\n', 'img/design/elena.jpg', 'The entire show is based around her. Elena is a slender, athletic seventeen year old girl. She has an olive complexion, brown eyes and long, straight, dark brown hair. Her parents died in a car crash in which she was the only survivor. She lives with her aunt and younger brother. She used to date Matt Donovan, but broke up with him after the passing of her parents. When school starts again she meets and falls in love with Stefan. But later, she meets Damon Salvatore, Stefan''s older brother . Elena is caught in the same love triangle Katherine was caught in.'),
(3, 'Stefan Salvatore', 'stefan\r\nStefan\r\nSalvatore\r\nsalvatore\r\nvampire\r\ncaring\r\nstrong', 'img/design/stefan.jpg\r\n', 'Stefan Salvatore was born during the Renaissance in Italy. He and his brother, Damon, were turned into vampires by the girl they both loved, Katherine. When Stefan discovered she was "dead." Damon and Stefan fought to the death. 500 years later Stefan decided to go to Fell''s Church, Virginia where he met Elena--who had a striking resemblance to Katherine. He tried to avoid Elena at all costs. But he couldn''t resist.'),
(4, 'Caroline Forbes', 'Caroline\r\nForbes\r\ncaroline\r\nforbes\r\npretty\r\nlovely\r\nfriendly\r\n', 'img/design/caroline.jpg', 'Caroline is the daughter of the sheriff of Mystic Falls, Liz Forbes, with whom she has a troubled relationship. Her father left her and her mother when he came clean about being gay. In the beginning she is shown as shallow, self-absorbed and jealous of Elena, however during the series Caroline becomes a nicer person. She later starts a relationship with Matt Donovan. In season two Caroline is turned into a vampire after drinking Damon''s blood, being killed by Katherine and unwillingly killing a boy and feeding on him.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`site_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `site_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
