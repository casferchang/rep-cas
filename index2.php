<?php


session_start();

if(!isset($_SESSION["login_user"])){
header("location: index.php");
} else {
?>	


<!DOCTYPE HTML>
<html>

<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>

  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="index2.php"> AIDA~~~</a></h1>
		<a class="right" href="logout.php">Log Out</a>
      </div>
      <div id="menubar">
	  
        <ul id="menu">
          <li class="current"><a href="index2.php">Home</a></li>
          <li><a href="oot2.php">OO Testing</a></li>
          <li><a href="about2.html">About Us</a></li>
          <li><a href="contact2.php">Contact Us</a></li>
		  	 
        </ul>
		
      </div>
    </div>
    <div id="site_content">
      <div id="sidebar_container">
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
        
        <h3>Welcome</h3>
         <p>Welcome to the PHP learning website</p>
		 <p>You may start access to the PHP example and learning</p>
		   <form method="post" action="#" id="subscribe">
           
			</form>
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Learning Output</h3>
		  <p>By learning PHP, you may know how to develop a wonderful website.</p>
		  <p>To create a website, several language need to realize immediately.</p>
		  
         
        </div>
        <img class="paperclip" src="style/paperclip.png" alt="paperclip" />
        <div class="sidebar">
          <h3>Notice</h3>
          <p>You can test your PHP skills <a href="https://www.codecademy.com/courses/web-beginner-en-bH5s3/0/1">here</a>.</p>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>PHP Introduction</h1>
     

		<p>PHP began its OOP life with PHP4, and has a pretty decent object model with PHP5. The release of PHP6 - who knows when? - 
		is destined to complete the object model nicely. </p>
		<p>The huge debate about whether OOP is better than procedural or not still rages on, 
		with no end in sight. Whether this debate will ever be concluded is another story altogether, but I strongly believe 
		that object oriented development is a better way to go. Perhaps this series of articles will show why. </p>
		<p>In this class we will explore the essential principles of object oriented programming (OOP) with PHP. Our goal is to
		 define what object oriented programming is, and what the advantages and disadvantages of using it are.</p>
        <p>In PHP we can define objects in similar ways; we can assign properties to them, as well as make them do things programmatically. 
		Obviously these objects will only exist in the program itself, but through user interfaces such as web pages, we can interact with 
		them and make them perform tasks and procedures, as well as calculate, retrieve and modify properties.
		<p>PHP gives us a very simple way to define an object programmatically, and this is called a class. A class is a wrapper that defines and 
		encapsulates the object along with all of its methods and properties. You can think of a class as a programmatic representation of an object,
		and it is the interface that PHP has given you, the developer, to interact with and modify the object. </p>
		
		
        <h2>Online Functions</h2>
		<ul>
          <li><a href="products.php"> Programming PHP Shop </a></li>
          <li><a href="upload.php">Upload Function</a></li>
          <li><a href="pdf.php">PDF Example</a></li>
        </ul>
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>

<?php
}

?>