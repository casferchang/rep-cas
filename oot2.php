
<?php
	
	if(isset($_SESSION['login_user'])){
		header("location: index.php");
		}
		//../ means go back to folder

	include "config/config.php";
	include "ObjectCollection.php";
	include "LineItem.php";
	include "Item.php";
	session_start();
	//if dont start session the items wont be saved
	$result = "";
	$mysqli_conn = new mysqli ($db['hostname'], $db ['username'], 
							$db['password'], $db ['database']);
	
	$result =$mysqli_conn ->query ("SELECT * FROM products");
	if($mysqli_conn -> connect_errno) {
    print "Faild to connect to MySQL: (" . 
	$mysqli_conn -> connect_errno . ")" . $mysqli_conn -> connect_error;
	}
	
?>

<!DOCTYPE HTML>
<html>
<head>
  <title>Object Oriented PHP</title>
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <h1>Object Oriented PHP<a href="oot2.php"> AIDA~~~</a></h1>
		<a class="right" href="logout.php">Log Out</a>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="current" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li class="current"><a href="oot2.php">OO Testing</a></li>
          <li><a href="about2.html">About Us</a></li>
          <li><a href="contact2.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="site_content">
			
		<div id="sidebar_container">
		<img class="paperclip" src="style/paperclip.png" alt="paperclip" />
		<div class="sidebar">
		<h3>Output</h3>
        <?php 
			//session part to display customer order
			$OC = new ObjectCollection();
			$IT = new Item("12",12);
			$LI = new LineItem($IT,10);
			
		
			if(isset($_SESSION["ObjColl"]))
			{
				$OC = $_SESSION["ObjColl"];
				$LI = $OC->getLineItem(0);
				echo "<br/><strong>Product Ordered: </strong><br/><i>".$OC->getLineCount()."</strong></i>";
				
				for($i=0;$i<$OC->getLineCount();$i++)
				{
					echo "<br/>Product Title: <i>".$OC->getLineItem($i)->getItem()->getTitle()."</i>";
					echo "<br/>Product Creator: <i>".$OC->getLineItem($i)->getItem()->getCreator()."</i>";
					echo "<br/>Product Price: <i>".$OC->getLineItem($i)->getItem()->getPrice()."</i>";
					echo "<br/>Product Description: <i>".$OC->getLineItem($i)->getItem()->getDesc()."</i>";
					echo "<br/>Product Image: <i>".$OC->getLineItem($i)->getItem()->getImg()."</i>";
					echo "<br/><strong>Quantity Ordered: <i>".$OC->getLineItem($i)->getQuantity()."</strong></i>";
					echo "<br/><br/>";	
				}
				
			}
			
?>
		</div>
	  </div>		
      <div id="content">
	  <h1>Object Oriented Testing</h1>
	
        <table border=1>
		<tr>
		<td><strong>Product</strong></td>
		<td><strong>Price</strong></td></tr>	
		<?php
		//displaying the product
			$counter=1;
				while($row = $result->fetch_assoc()) {
					print '<tr><td><a href="displayPage.php?id='.$row["id"].'">'.$row["title"].'</a></td>';
					print "<td>".$row["price"]."</td>";
					$counter++;
				}
				
				
		?>
		</table>
      </div>
    </div>
    <div id="footer">
      <p>Copyright &copy; Design <a href="https://www.facebook.com/mister.casfer">Casfer </a></p>
    </div>
  </div>
</body>
</html>